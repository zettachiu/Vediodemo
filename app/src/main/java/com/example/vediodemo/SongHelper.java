package com.example.vediodemo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;


public class SongHelper extends SQLiteOpenHelper {


    public SongHelper(@Nullable Context context) {
        super(context, "songdatabase.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table songs(song_name varchar(20),song_singer varchar(20),music_res varchar(20),mv_res varchar(20))";
        db.execSQL(sql);
        String sql1="insert into songs(song_name,song_singer,music_res,mv_res) values ('Chiu','1391575453@qq.com','123123','cai')";
        db.execSQL(sql1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
