package com.example.vediodemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.vediodemo.bean.Song;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    SongHelper songHelper;
    RecyclerView recyclerView;
    SongAdpater songAdpater;
    SongTools songTools;
    SQLiteDatabase db;
    List songs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        songAdpater = new SongAdpater(MainActivity.this,songs);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(songAdpater);

    }

    private void initView(){
        songHelper = new SongHelper(this);
        recyclerView = findViewById(R.id.recv);
        songTools = new SongTools(MainActivity.this);
        songs = new ArrayList<Song>();
        String sql = "select * from songs";
        db = songHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from songs",null);
        if (cursor == null){
            return;
        }
        while (cursor.moveToNext()){
            int a=cursor.getColumnIndex("mv_res");
            String mv_res = cursor.getString(a);
            Song song = new Song(mv_res);
            songs.add(song);
        }
        cursor.close();
    }




}