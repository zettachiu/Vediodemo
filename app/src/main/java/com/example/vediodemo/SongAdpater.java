package com.example.vediodemo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vediodemo.bean.Song;

import java.util.List;

public class SongAdpater extends RecyclerView.Adapter<SongAdpater.SongViewHolder>{
    List<Song> songList;
    Context context;
    Vedioplay vedioplay;
    String mv_res;



    public SongAdpater(Context context, List<Song> songList){
        this.songList = songList;
        this.context = context;
    }

    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_main_item,parent,false);
        SongViewHolder songViewHolder= new SongViewHolder(view);
        return songViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {

        Song song = songList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mv_res = song.getMv_res();
                Intent intent = new Intent(context,Vedioplay.class);
                intent.putExtra("mv_res",mv_res);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }





    class SongViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public SongViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView3);
        }
    }


}
