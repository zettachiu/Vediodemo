package com.example.vediodemo.bean;

public class Song {
    private String song_name,song_singer,music_res,mv_res;

    public Song(String song_name, String song_singer, String music_res, String mv_res) {
        this.song_name = song_name;
        this.song_singer = song_singer;
        this.music_res = music_res;
        this.mv_res = mv_res;
    }
    public Song(String mv_res){

    }

    public String getSong_name() {
        return song_name;
    }

    public void setSong_name(String song_name) {
        this.song_name = song_name;
    }

    public String getSong_singer() {
        return song_singer;
    }

    public void setSong_singer(String song_singer) {
        this.song_singer = song_singer;
    }

    public String getMusic_res() {
        return music_res;
    }

    public void setMusic_res(String music_res) {
        this.music_res = music_res;
    }

    public String getMv_res() {
        return mv_res;
    }

    public void setMv_res(String mv_res) {
        this.mv_res = mv_res;
    }
}
