package com.example.vediodemo;

import android.net.Uri;
import android.os.Bundle;
import android.widget.Adapter;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class Vedioplay extends AppCompatActivity {
    VideoView videoView;
    String url;
    MediaController mediaController;
    String music_name;
    String mv_res;

//    public Vedioplay(String music_name){
//        this.music_name = music_name;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vedioplay);

        videoView = findViewById(R.id.activity_vedio_play);
        mv_res = getIntent().getStringExtra("mv_res");
        url = "android.resource://com.example.vediodemo/raw/"+mv_res;
        Uri uri = Uri.parse(url);
        videoView.setVideoURI(uri);
        mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        if(videoView!=null&&videoView.isPlaying()){
            videoView.stopPlayback();
            return;
        }
        videoView.start();
    }



}